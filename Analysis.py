from ROOT import *
from glob import glob
from array import array


# Select FE of Reference Signal 
def RefStyleCut(tree,hitIndexList):  # StyleStamp: LE = 1 , FE = 0 
    newHitList = []
    for index in hitIndexList:
        if 75 <= tree.TDC_channel[index] <= 86 and tree.TDC_StyleStamp[index] == 1:
            newHitList.append(index)
        if 5 <= tree.TDC_channel[index] <= 10 or 21 <= tree.TDC_channel[index] <= 26:
            newHitList.append(index)
    return newHitList

# Signal Time Cut
def TimeCut(tree,hitIndexList):
    newHitList = []
    for index in hitIndexList:
        if((5 <= tree.TDC_channel[index] <= 10 or 21 <= tree.TDC_channel[index] <= 26) and 320 < tree.TDC_TimeStamp[index] < 400):
            newHitList.append(index)
        if(75 <= tree.TDC_channel[index] <= 86 and 340 <= tree.TDC_TimeStamp[index] <= 410):
            newHitList.append(index)
    return newHitList
            
#divide the reference RPC middle region into three regions to check flatness
def RegionClassify(tree,hitIndexList):
    # DoubleEnd RPC Region: 5 ~ 10、21 ~ 26
    RPCLeftIndexList = []            
    RPCRightIndexList = []
    # Reference RPC Region:75 ~ 86  Noise Channel : 87         
    RefRegion1IndexList = []         # Region1:75 ~ 78   
    RefRegion2IndexList = []         # Region2:79 ~ 82
    RefRegion3IndexList = []         # Region1:83 ~ 86        
    for index in hitIndexList: 
        if(5 <= tree.TDC_channel[index] <= 10):              
            RPCLeftIndexList.append(index)
        if(21 <= tree.TDC_channel[index] <= 26):             
            RPCRightIndexList.append(index)
        if(75 <= tree.TDC_channel[index] <= 78):
            RefRegion1IndexList.append(index)
        if(79 <= tree.TDC_channel[index] <= 82):
            RefRegion2IndexList.append(index)
        if(83 <= tree.TDC_channel[index] <= 86):
            RefRegion3IndexList.append(index)
    return RPCLeftIndexList,RPCRightIndexList,RefRegion1IndexList,RefRegion2IndexList,RefRegion3IndexList

#classify reference signal and test double-end RPC signal
def TestStyleClassify(tree,RPCLeftIndexList,RPCRightIndexList):
    LeftLEIndexList = []
    LeftFEIndexList = []
    RightLEIndexList = []
    RightFEIndexList = []
    for index in RPCLeftIndexList:
        if tree.TDC_StyleStamp[index] == 1:
            LeftLEIndexList.append(index)
        if tree.TDC_StyleStamp[index] == 0:
            LeftFEIndexList.append(index)
    for index in RPCRightIndexList:
        if tree.TDC_StyleStamp[index] == 1:
            RightLEIndexList.append(index)
        if tree.TDC_StyleStamp[index] == 0:
            RightFEIndexList.append(index)
    return LeftLEIndexList,LeftFEIndexList,RightLEIndexList,RightFEIndexList

# Only one side of the double-end RPC have signal(broader selection)
def IsOrTriggerA(LeftLEIndexList,LeftFEIndexList,RightLEIndexList,RightFEIndexList):
    IsOrTrigger = False
    if (len(LeftLEIndexList) > 0 and len(LeftFEIndexList) > 0) or (len(RightLEIndexList) > 0 and len(RightFEIndexList) > 0):
        IsOrTrigger = True
    return IsOrTrigger

# Two side of the double-end RPC all have signals
def IsAndTriggerA(LeftLEIndexList,LeftFEIndexList,RightLEIndexList,RightFEIndexList):
    IsAndTrigger = False
    if (len(LeftLEIndexList) > 0 and len(LeftFEIndexList) > 0) and (len(RightLEIndexList) > 0 and len(RightFEIndexList) > 0):
        IsAndTrigger = True
    return IsAndTrigger

# Find LE and FE of double-end signal and calculate ToT
def LEAndFEAndToTCal(tree,LeftLEIndexList,LeftFEIndexList,RightLEIndexList,RightFEIndexList):
    LeftLETime = 0
    LeftFETime = 0 
    RightLETime = 0
    RightFETime = 0
    LeftToT = 0
    RightToT = 0
    if len(LeftLEIndexList) == 1:
        LeftLETime = tree.TDC_TimeStamp[LeftLEIndexList[0]]
    if len(LeftLEIndexList) > 1:
        LeftLETime = tree.TDC_TimeStamp[LeftLEIndexList[0]]
        for index in LeftLEIndexList:
            if tree.TDC_TimeStamp[index] < LeftLETime:
                LeftLETime = tree.TDC_TimeStamp[index] 
    if len(LeftFEIndexList) == 1:
        LeftFETime = tree.TDC_TimeStamp[LeftFEIndexList[0]]
    if len(LeftFEIndexList) > 1:
        LeftFETime = tree.TDC_TimeStamp[LeftFEIndexList[0]]
        for index in LeftFEIndexList:
            if tree.TDC_TimeStamp[index] < LeftFETime:
                LeftFETime = tree.TDC_TimeStamp[index]
    if len(RightLEIndexList) == 1:
        RightLETime = tree.TDC_TimeStamp[RightLEIndexList[0]]
    if len(RightLEIndexList) > 1:
        RightLETime = tree.TDC_TimeStamp[RightLEIndexList[0]]
        for index in RightLEIndexList:
            if tree.TDC_TimeStamp[index] < RightLETime:
                RightLETime = tree.TDC_TimeStamp[index] 
    if len(RightFEIndexList) == 1:
        RightFETime = tree.TDC_TimeStamp[RightFEIndexList[0]]
    if len(RightFEIndexList) > 1:
        RightFETime = tree.TDC_TimeStamp[RightFEIndexList[0]]
        for index in RightFEIndexList:
            if tree.TDC_TimeStamp[index] < RightFETime:
                RightFETime = tree.TDC_TimeStamp[index]
    if LeftLETime > 0 and LeftFETime > 0 :
        LeftToT = LeftFETime - LeftLETime
    if RightLETime > 0 and RightFETime > 0 :    
        RightToT = RightFETime - RightLETime
    return LeftToT,RightToT

#Calculate ClusterSize
def ClusterSizeCal(LeftLEIndexList,LeftFEIndexList,RightLEIndexList,RightFEIndexList):
    ClusterSize = 0
    if len(LeftLEIndexList) == 1 and len(LeftFEIndexList) == 1 and len(RightLEIndexList) == 1 and len(RightFEIndexList) == 1:
        ClusterSize = 1
    if len(LeftLEIndexList) < 6 and len(LeftFEIndexList) < 6 and len(RightLEIndexList) < 6 and len(RightFEIndexList) < 6:
      CSLeft = len(LeftFEIndexList)
      CSRight = len(RightFEIndexList)
      if CSLeft < CSRight:
          ClusterSize = CSLeft
      if CSLeft >= CSRight:
          ClusterSize = CSRight
    return ClusterSize




if __name__ == '__main__':
    filename = 'GasGapTest_DoubleEnd_HV6200_Ref6000_6000_run20220822144043.root'
    f1 = TFile.Open(filename)
    tree = f1.Get('RAWData')
    RefRegion1OrNum = 0
    TestRegion1OrNum = 0
    RefRegion2OrNum = 0
    TestRegion2OrNum = 0
    RefRegion3OrNum = 0
    TestRegion3OrNum = 0
    RefRegion1AndNum = 0
    TestRegion1AndNum = 0
    RefRegion2AndNum = 0
    TestRegion2AndNum = 0
    RefRegion3AndNum = 0
    TestRegion3AndNum = 0
    ClusterSizeDis = TH1F("ClusterSizeDis","ClusterSizeDis",6,0,6)
    LeftToTDis = TH1F("LeftToTDis","LeftToTDis",30,10,40)
    RightToTDis = TH1F("RightToTDis","RightToTDis",30,10,40)
    # Calculate real HV
    Temperature = 297.95
    GasPressure = 1010.8
    RefGasPressure = 950
    RefTemperature = 293
    HVfactor = (1+0.8*((GasPressure - RefGasPressure)/RefGasPressure))*(1-0.5*((Temperature- RefTemperature)/Temperature))
    AppliedHV = 6200
    EffHV = AppliedHV/HVfactor
    print('EffHV = ',EffHV)
    for entry in range(int(tree.GetEntries())):
    #for entry in range(40):
        tree.GetEntry(entry)
        hitIndexList = list(range(tree.number_of_hits))
        hitIndexList = RefStyleCut(tree,hitIndexList)
        hitIndexList = TimeCut(tree,hitIndexList)
        RPCLeftIndexList,RPCRightIndexList,RefRegion1IndexList,RefRegion2IndexList,RefRegion3IndexList = RegionClassify(tree,hitIndexList)
        LeftLEIndexList,LeftFEIndexList,RightLEIndexList,RightFEIndexList = TestStyleClassify(tree,RPCLeftIndexList,RPCRightIndexList)
        IsOrTrigger = IsOrTriggerA(LeftLEIndexList,LeftFEIndexList,RightLEIndexList,RightFEIndexList)
        IsAndTrigger = IsAndTriggerA(LeftLEIndexList,LeftFEIndexList,RightLEIndexList,RightFEIndexList)
        LeftToT,RightToT = LEAndFEAndToTCal(tree,LeftLEIndexList,LeftFEIndexList,RightLEIndexList,RightFEIndexList)
        LeftToTDis.Fill(LeftToT)
        RightToTDis.Fill(RightToT)
        ClusterSize = ClusterSizeCal(LeftLEIndexList,LeftFEIndexList,RightLEIndexList,RightFEIndexList)
        ClusterSizeDis.Fill(ClusterSize)
        if len(RefRegion1IndexList) > 0 and len(RefRegion2IndexList) == 0 and len(RefRegion3IndexList) == 0:
            RefRegion1OrNum += 1
            if len(RPCLeftIndexList) > 0 and IsOrTrigger == True:
                TestRegion1OrNum += 1
        if len(RefRegion2IndexList) > 0 and len(RefRegion1IndexList) == 0 and len(RefRegion3IndexList) == 0:
            RefRegion2OrNum += 1
            if len(RPCLeftIndexList) > 0 and IsOrTrigger == True:
                TestRegion2OrNum += 1
        if len(RefRegion3IndexList) > 0 and len(RefRegion2IndexList) == 0  and len(RefRegion1IndexList) == 0:
            RefRegion3OrNum += 1
            if len(RPCLeftIndexList) > 0 and IsOrTrigger == True:
                TestRegion3OrNum += 1
        if len(RefRegion1IndexList) > 0 and len(RefRegion2IndexList) == 0 and len(RefRegion3IndexList) == 0:
            RefRegion1AndNum += 1
            if len(RPCLeftIndexList) > 0 and IsAndTrigger == True:
                TestRegion1AndNum += 1
        if len(RefRegion2IndexList) > 0 and len(RefRegion1IndexList) == 0 and len(RefRegion3IndexList) == 0:
            RefRegion2AndNum += 1
            if len(RPCLeftIndexList) > 0 and IsAndTrigger == True:
                TestRegion2AndNum += 1
        if len(RefRegion3IndexList) > 0 and len(RefRegion2IndexList) ==0  and len(RefRegion1IndexList) == 0:
            RefRegion3AndNum += 1
            if len(RPCLeftIndexList) > 0 and IsAndTrigger == True:
                TestRegion3AndNum += 1
    print("Or: ",RefRegion1OrNum,RefRegion2OrNum,RefRegion3OrNum,int(tree.GetEntries()))
    print("Region1 Eff Or: ",TestRegion1OrNum/RefRegion1OrNum,"Region2 Eff Or: ",TestRegion2OrNum/RefRegion2OrNum,"Region3 Eff Or: ",TestRegion3OrNum/RefRegion3OrNum)
    print("And: ",RefRegion1AndNum,RefRegion2AndNum,RefRegion3AndNum,int(tree.GetEntries()))
    print("Region1 Eff And: ",TestRegion1AndNum/RefRegion1AndNum,"Region2 Eff And: ",TestRegion2AndNum/RefRegion2AndNum,"Region3 Eff And: ",TestRegion3AndNum/RefRegion3AndNum)


c3 = TCanvas("c3", "BPRE", 800, 600)
c3.cd()
c3.Divide(1,1,0.008,0.007)
gPad.SetTopMargin(0.09)
gPad.SetBottomMargin(0.10)
gPad.SetLeftMargin(0.10)
gPad.SetRightMargin(0.05)

LeftToTDis.SetLineColor(kBlue)
LeftToTDis.Draw()
ax = LeftToTDis.GetXaxis()
#ax.SetRangeUser(SignalStartBin, SignalEndBin)
ax.SetTitle( " Time (ns) " )
ay = LeftToTDis.GetYaxis()
ay.SetTitle( "Entries" )
ay.SetTitleSize(0.04)
ax.SetTitleOffset(1.0)
ay.SetTitleOffset(0.8)
ax.SetTitleSize(0.04)
ax.SetLabelSize(0.04)
ay.SetLabelSize(0.04)
ax.Draw("same")
ay.Draw("same")
c3.SaveAs("LeftToTDis.png")

RightToTDis.SetLineColor(kBlue)
RightToTDis.Draw()
ax = RightToTDis.GetXaxis()
#ax.SetRangeUser(SignalStartBin, SignalEndBin)
ax.SetTitle( " Time (ns) " )
ay = RightToTDis.GetYaxis()
ay.SetTitle( "Entries" )
ay.SetTitleSize(0.04)
ax.SetTitleOffset(1.0)
ay.SetTitleOffset(0.8)
ax.SetTitleSize(0.04)
ax.SetLabelSize(0.04)
ay.SetLabelSize(0.04)
ax.Draw("same")
ay.Draw("same")
c3.SaveAs("RightToTDis.png")

ClusterSizeDis.SetLineColor(kBlue)
ClusterSizeDis.Draw()
ax = ClusterSizeDis.GetXaxis()
#ax.SetRangeUser(SignalStartBin, SignalEndBin)
ax.SetTitle( " Position (mm) " )
ay = ClusterSizeDis.GetYaxis()
ay.SetTitle( "Entries" )
ay.SetTitleSize(0.04)
ax.SetTitleOffset(1.0)
ay.SetTitleOffset(0.8)
ax.SetTitleSize(0.04)
ax.SetLabelSize(0.04)
ay.SetLabelSize(0.04)
ax.Draw("same")
ay.Draw("same")
c3.SaveAs("ClusterSizeDis.png")

    
           
       



